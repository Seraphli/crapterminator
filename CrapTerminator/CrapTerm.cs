﻿using System.IO;

namespace CrapTerminator
{
    public class CrapTerm
    {
        string[] blacklist, graylist;
        int morality = 10;
        int lowMorality = 8, grayWordPerSen = 2;
        public CrapTerm()
        {
            loadList();
        }

        void loadList()
        {
            blacklist = File.ReadAllLines("BlackList.txt");
            graylist = File.ReadAllLines("GrayList.txt");
        }

        public string crapOut(string msg)
        {
            bool changed = false;
            for (int i = 0; i < blacklist.Length; i++)
            {
                var word = blacklist[i];
                if (msg.Contains(word))
                {
                    if (morality > 0)
                    {
                        morality -= 2;
                    }
                    var star = "";
                    for (int j = 0; j < word.Length; j++)
                    {
                        star += "*";
                    }
                    msg = msg.Replace(word, star);
                    changed = true;
                }
            }
            int count = 0;
            bool singleword = false;
            for (int i = 0; i < graylist.Length; i++)
            {
                var word = graylist[i];
                if (msg.Contains(word))
                {
                    
                    if (msg.Length == word.Length)
                    {
                        singleword = true;
                        break;
                    }
                    else
                    {
                        int start = msg.IndexOf(word);
                        int end = start + word.Length;
                        var temp1 = start == 0 ? ' ' : msg.ToCharArray(start - 1, 1)[0];
                        var temp2 = msg.ToCharArray(start + word.Length, 1)[0];
                        if ((useless(temp1) && useless(temp2)))
                        {
                            singleword = true;
                            break;
                        }
                    }
                    count++;
                    if (count >= 2)
                    {
                        break;
                    }
                }
            }
            if (morality < lowMorality || count >= grayWordPerSen || singleword)
            {
                for (int i = 0; i < graylist.Length; i++)
                {
                    var word = graylist[i];
                    if (msg.Contains(word))
                    {
                        if (morality > 0)
                        {
                            morality -= 1;
                        }
                        var star = "";
                        for (int j = 0; j < word.Length; j++)
                        {
                            star += "*";
                        }
                        msg = msg.Replace(word, star);
                        changed = true;
                    }
                }
            }
            if (!changed && morality < 10)
            {
                morality++;
            }
            return msg;
        }

        public bool useless(char s)
        {
            if (s < '0')
                return true;
            if (s > '9' && s < 'A')
                return true;
            if (s > 'Z' && s < 'a')
                return true;
            if (s > 'z' && s < 128)
                return true;
            return false;
        }

        public string filter(string msg)
        {

            return msg;
        }
    }
}
